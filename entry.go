package qupid

import (
	"sync"
	"time"
)

type (
	// Entry is a Queue entry/item that stores an ID and processing times, along with an optional entity.
	Entry struct {
		ID         string
		Entity     interface{}
		processing time.Time
		completed  time.Time
		sync.Mutex
	}

	// EntryOption is a func type to assign options to a new Entry pointer.
	EntryOption func(*Entry)
)

// WithEntity will assign an entity to the queue entry.
func WithEntity(entity interface{}) EntryOption {
	return func(e *Entry) {
		e.Entity = entity
	}
}

// NewEntry will return a new entry pointer instance. Pass a required ID, and optional options.
func NewEntry(id string, eos ...EntryOption) *Entry {
	e := &Entry{ID: id}
	for _, eo := range eos {
		eo(e)
	}
	return e
}

// IsCompleted will return whether the entry is deemed to be processed/completed.
func (e *Entry) IsCompleted() bool {
	e.Lock()
	defer e.Unlock()
	return !e.completed.IsZero()
}

// IsProcessing will return whether the entry is currently being processed.
func (e *Entry) IsProcessing() bool {
	e.Lock()
	defer e.Unlock()
	return !e.processing.IsZero() && e.completed.IsZero()
}
