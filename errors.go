package qupid

// QupidError is the error struct used for the qupid package.
type QupidError struct {
	s string
}

var (
	// ErrQupidProcessingStopped denotes the queue has been stopped so no queuing can occur.
	ErrQupidProcessingStopped = NewQupidError("Queue processing has been stopped.")

	// ErrQupidPipelineBufferFull denotes that the queue pipeline is full and cannot add anymore entries.
	ErrQupidPipelineBufferFull = NewQupidError("Queue pipeline buffer is full. No longer adding queued items.")
)

// Error will return the string representation of the error instance.
func (e QupidError) Error() string {
	return e.s
}

// IsQupidError will confirm with the error instance is of type QupidError.
func IsQupidError(err error) bool {
	_, ok := err.(*QupidError)
	return ok
}

// NewQupidError will return a new QupidError pointer instance.
func NewQupidError(errorMessage string) error {
	return &QupidError{errorMessage}
}
