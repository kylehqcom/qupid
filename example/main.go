package main

import (
	"errors"
	"fmt"
	"math/rand"
	"strconv"
	"time"

	"gitlab.com/kylehqcom/qupid"
)

var q = qupid.NewQueue(qupid.WithPipelineBuffer(1000), qupid.WithPruneAfter(time.Minute))

// Do will add, consume and send results to the queue. Plus stop/return when invoked.
func Do() {
	t := time.NewTicker(time.Millisecond * 100)
	go func() {
		for {
			select {
			case <-t.C:
				ent, err := ToProcess()
				if err != nil {
					Stop()
					return
				}
				fmt.Println("Adding an entry to the queue with Name:", ent.Name)
				q.Queue(qupid.NewEntry(ent.ID, qupid.WithEntity(ent)))

			case e := <-q.Consume():
				// In general you never want your consumer to be blocking so
				// place is a goroutine as required.
				go func(ee *qupid.Entry) {
					res := qupid.NewResult(ee)
					ent, ok := ee.Entity.(*Ent)
					if !ok {
						res.Err = errors.New("WTF?")
					}
					fmt.Println("Consuming from the queue with Name:", ent.Name)
					q.Complete(res)
				}(e)
			case res := <-q.Results():
				fmt.Println("Yah a result came through for ID:", res.Entry.ID)

			case <-q.Interrupt():
				t.Stop()
				return
			}
		}
	}()
}

// Ent is an example entity struct.
type Ent struct {
	ID   string
	Name string
}

var r = rand.New(rand.NewSource(99))

// ToProcess will simply return a new entity struct for the example.
func ToProcess() (*Ent, error) {
	id := strconv.Itoa(int(r.Int63()))
	return &Ent{
		ID:   id,
		Name: "bar-" + id,
	}, nil
}

// Stop will invoke a q.Stop()
func Stop() {
	fmt.Println("Calling stop")
	q.Stop()
}

func main() {
	defer Stop()
	Do()
	time.Sleep(time.Second * 1)
}
