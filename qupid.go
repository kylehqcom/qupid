package qupid

import (
	"sync"
	"time"
)

const (
	completed          qName         = "completed"
	queued             qName         = "queued"
	pipelineBuffer     int           = 1000
	fallbackPruneAfter time.Duration = time.Hour
	fallbackPruneEvery time.Duration = time.Minute * 5
)

type (
	// qName is a string type to name internal processing queues.
	qName string

	// iQueue is an internal queue used for processing.
	iQueue struct {
		queued map[string]*Entry
	}

	// Q is the default q struct which holds internal queue data, along with the appropriate channels
	// for callers to select on.
	Q struct {
		pipeline struct {
			work    chan *Entry
			result  chan result
			stop    chan struct{}
			stopped bool
		}
		sync.Mutex
		qopts  qOptions
		queues map[qName]*iQueue
	}

	// qOptions configure a new Queue. qOptions are set by the QOption
	// values passed to NewQueue.
	qOptions struct {
		iQueuesDisabled bool
		pipelineBuffer  int
		pruneAfter      time.Duration
		pruneEvery      time.Duration
	}

	// QOption is a single option to pass to a NewQueue call.
	QOption func(*qOptions)
)

// WithPipelineBuffer assigns the channel buffer to the working pipeline. If none passed, the pipelineBuffer const
// is used by default.
func WithPipelineBuffer(buf int) QOption {
	return func(o *qOptions) {
		o.pipelineBuffer = buf
	}
}

// WithInternalTrackingDisabled will disable handling internal queues. Useful when just using Qupid as a simple
// channel/stop mechanism. When disabled the internal queue maps will not be populated or checked for dupes.
func WithInternalTrackingDisabled() QOption {
	return func(o *qOptions) {
		o.iQueuesDisabled = true
	}
}

// WithPruneAfter will periodically check and prune completed queue entries after the time duration specified. Only
// applicable when WithInternalTrackingDisabled is false (the default).
func WithPruneAfter(e time.Duration) QOption {
	return func(o *qOptions) {
		o.pruneAfter = e
	}
}

// WithPruneEvery will call prune on the queue instance based on the duration given. A zero duration will
// default to pruneEvery 5 mins.
func WithPruneEvery(e time.Duration) QOption {
	return func(o *qOptions) {
		o.pruneEvery = e
	}
}

// NewQueue will create a new Q pointer, passing in QOptions as required.
// Example: NewQueue(WithPipelineBuffer(10), WithPruneAfter(time.Second * 5))
func NewQueue(opts ...QOption) *Q {
	q := &Q{
		queues: make(map[qName]*iQueue),
	}

	// Apply any/all q options.
	for _, opt := range opts {
		opt(&q.qopts)
	}

	// Assign the default buffer if not set.
	if q.qopts.pipelineBuffer == 0 {
		q.qopts.pipelineBuffer = pipelineBuffer
	}
	q.pipeline.work = make(chan *Entry, q.qopts.pipelineBuffer)
	q.pipeline.result = make(chan result, q.qopts.pipelineBuffer)
	q.pipeline.stop = make(chan struct{})

	// Is internal queue tracking disabled?
	if !q.qopts.iQueuesDisabled {
		cq := &iQueue{}
		cq.queued = make(map[string]*Entry)
		q.queues[completed] = cq

		qq := &iQueue{}
		qq.queued = make(map[string]*Entry)
		q.queues[queued] = qq

		pruneEvery := fallbackPruneEvery
		if 0 < q.qopts.pruneEvery {
			pruneEvery = q.qopts.pruneEvery
		}

		// Setup the queue pruner if a valid value set.
		if 0 < q.qopts.pruneAfter {
			go func() {
				// Look to prune items every pruneEvery.
				t := time.NewTicker(pruneEvery)
				for {
					select {
					case <-t.C:
						q.Prune(q.qopts.pruneAfter)
					case <-q.Interrupt():
						t.Stop()
						return
					}
				}
			}()
		}
	}

	return q
}

// Complete marks an entry as complete, moves the entry to the completed queue and sends the result down the channel.
func (q *Q) Complete(r result) {
	// Is internal queue tracking disabled?
	if !q.qopts.iQueuesDisabled {
		e := q.get(r.Entry.ID)
		if e == nil {
			return
		}

		q.Lock()
		defer q.Unlock()
		qd := q.queues[queued]
		qd.remove(e)

		c := q.queues[completed]
		e.completed = time.Now()
		c.add(e)
	}

	// Add the result via a goroutine to ensure non blocking.
	go func() {
		q.pipeline.result <- r
	}()
}

// Consume returns the channel which you consume queue entries from.
func (q *Q) Consume() <-chan *Entry {
	return q.pipeline.work
}

// get will return an entry based on the id used to assign to an internal queue.
func (q *Q) get(id string) *Entry {
	q.Lock()
	defer q.Unlock()
	for _, qs := range q.queues {
		if e, ok := qs.queued[id]; ok {
			return e
		}
	}

	return nil
}

// Interrupt returns the channel which you await for a stop/interrupt event to occur.
func (q *Q) Interrupt() <-chan struct{} {
	return q.pipeline.stop
}

// Prune will remove completed entries from the completed queue when a "safe" time
// duration has passed. If tracking internal queues, Prune should be called periodically
// to continually clean up.
func (q *Q) Prune(after time.Duration) {
	// Short circuit when not tracking internal queues.
	if q.qopts.iQueuesDisabled {
		return
	}

	pruneAfter := fallbackPruneAfter
	if after > 0 {
		pruneAfter = after
	}

	c := q.queues[completed]
	expire := time.Now().Add(-pruneAfter)
	for _, e := range c.queued {
		if e.completed.Before(expire) {
			c.remove(e)
		}
	}
}

// Queue will attempt to assign an entry to the queue for processing. Will return an error if queue processing
// is stopped or the queue buffer is full. If internal queue tracking is enabled, which is the default, an entry
// will be ignored if already present on the completed queue. On no error or no dupe, the entry is added to the
// pipeline for processing.
func (q *Q) Queue(e *Entry) error {
	q.Lock()
	defer q.Unlock()
	if q.pipeline.stopped {
		return ErrQupidProcessingStopped
	}

	if !q.qopts.iQueuesDisabled {

		// Don't bother adding a new entry if exists on completed. If the entry does need re-running
		// it will be picked up after queue pruning.
		cd := q.queues[completed]
		if _, ok := cd.queued[e.ID]; ok {
			return nil
		}
	}

	// Now send the entry down the channel, non blocking
	select {
	case q.pipeline.work <- e:
		if !q.qopts.iQueuesDisabled {
			// Add the entry to the queued queue.
			e.processing = time.Now()
			q.queues[queued].add(e)
		}
	default:
		return ErrQupidPipelineBufferFull
	}

	return nil
}

// Results returns the channel which you await for completed work results.
func (q *Q) Results() <-chan result {
	return q.pipeline.result
}

// Remove will remove an entry from this Q instance.
func (q *Q) Remove(e *Entry) {
	q.Lock()
	defer q.Unlock()
	for _, iq := range q.queues {
		iq.remove(e)
	}
}

// RemoveByID will remove an entry from this Q instance based on its string ID.
func (q *Q) RemoveByID(id string) {
	e := q.get(id)
	if e != nil {
		q.Remove(e)
	}
}

// Stop will close the queue pipeline and mark the queue as stopped.
func (q *Q) Stop() {
	q.Lock()
	defer q.Unlock()
	if !q.pipeline.stopped {
		close(q.pipeline.stop)
	}
	q.pipeline.stopped = true
}

func (iq *iQueue) add(e *Entry) {
	iq.queued[e.ID] = e
}

func (iq *iQueue) Len() int {
	return len(iq.queued)
}

func (iq *iQueue) remove(e *Entry) {
	delete(iq.queued, e.ID)
}
