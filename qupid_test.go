package qupid

import (
	"errors"
	"reflect"
	"testing"
	"time"
)

func TestWithPipelineBuffer(t *testing.T) {
	qopts := &qOptions{}
	if 0 != qopts.pipelineBuffer {
		t.Error("0 is the default value of the pipeline buffer.")
	}

	WithPipelineBuffer(5)(qopts)
	if 5 != qopts.pipelineBuffer {
		t.Error("Setting pipeline buffer fail.")
	}
}

func TestWithInternalTrackingDisabled(t *testing.T) {
	qopts := &qOptions{}
	if qopts.iQueuesDisabled {
		t.Error("Disabled internal queue tracking should be false by default.")
	}

	WithInternalTrackingDisabled()(qopts)
	if !qopts.iQueuesDisabled {
		t.Error("Disabled internal queue tracking should be true.")
	}
}

func TestWithPruneAfter(t *testing.T) {
	qopts := &qOptions{}
	if qopts.pruneAfter.Nanoseconds() != 0 {
		t.Error("Prune after should be zero by default.")
	}

	WithPruneAfter(time.Second)(qopts)
	if qopts.pruneAfter != time.Second {
		t.Error("Prune after should be 1 second.")
	}
}

func TestWithPruneEvery(t *testing.T) {
	qopts := &qOptions{}
	if qopts.pruneEvery.Nanoseconds() != 0 {
		t.Error("Prune every should be zero by default.")
	}

	WithPruneEvery(time.Second)(qopts)
	if qopts.pruneEvery != time.Second {
		t.Error("Prune every should be 1 second.")
	}
}

func TestNewQueue(t *testing.T) {
	defaultOpts := qOptions{
		pipelineBuffer: pipelineBuffer,
	}

	someOpts := qOptions{
		pipelineBuffer: 2,
		pruneAfter:     time.Hour,
		pruneEvery:     time.Hour,
	}

	fullOpts := qOptions{
		pipelineBuffer:  5,
		pruneAfter:      time.Second,
		pruneEvery:      time.Second,
		iQueuesDisabled: true,
	}

	tests := []struct {
		want qOptions
		in   *Q
	}{
		{
			want: defaultOpts,
			in:   NewQueue(),
		},
		{
			want: someOpts,
			in: NewQueue(
				WithPipelineBuffer(2),
				WithPruneAfter(time.Hour),
				WithPruneEvery(time.Hour),
			),
		},
		{
			want: fullOpts,
			in: NewQueue(
				WithPipelineBuffer(5),
				WithPruneAfter(time.Second),
				WithPruneEvery(time.Second),
				WithInternalTrackingDisabled(),
			),
		},
	}

	for _, tt := range tests {
		if tt.in.qopts.iQueuesDisabled != tt.want.iQueuesDisabled {
			t.Error("Internal queue disabled mismatch.")
		}

		if tt.in.qopts.pipelineBuffer != tt.want.pipelineBuffer {
			t.Error("Pipeline buffer mismatch.")
		}

		if tt.in.qopts.pruneAfter != tt.want.pruneAfter {
			t.Error("Prune after value mismatch.")
		}

		if tt.in.qopts.pruneEvery != tt.want.pruneEvery {
			t.Error("Prune every value mismatch.")
		}
	}
}

func TestQ_Complete(t *testing.T) {
	q := NewQueue()
	if 0 != q.queues[completed].Len() {
		t.Error("Completed queue length should be zero.")
	}

	id := "foo"
	q.Complete(NewResult(NewEntry(id)))
	if 0 != q.queues[completed].Len() {
		t.Error("Completed queue length should be zero on unknown entry.")
	}

	e := NewEntry(id)
	q.queues[queued].add(e)
	q.Complete(NewResult(e))
	tk := time.NewTicker(time.Second)
	for {
		select {
		case <-q.Results():
			tk.Stop()
			goto confirm

		case <-tk.C:
			t.Error("Results channel not consumed on complete.")
			return
		}
	}

confirm:
	if 1 != q.queues[completed].Len() {
		t.Error("Completed queue length should be one.")
	}
}

func TestQ_Consume(t *testing.T) {
	q := NewQueue()
	cs := q.Consume()
	if "*qupid.Entry" != reflect.TypeOf(cs).Elem().String() {
		t.Error("Unexpected chan returned.")
	}

	if "chan" != reflect.TypeOf(cs).Kind().String() {
		t.Error("Unexpected type returned.")
	}
}

func TestQ_Interrupt(t *testing.T) {
	q := NewQueue()
	i := q.Interrupt()
	if "struct {}" != reflect.TypeOf(i).Elem().String() {
		t.Error("Unexpected chan returned.")
	}

	if "chan" != reflect.TypeOf(i).Kind().String() {
		t.Error("Unexpected type returned.")
	}
}

func TestQ_Prune(t *testing.T) {
	e := NewEntry("boom")
	e.completed = time.Now().Add(time.Second * -5)
	q := NewQueue()
	q.queues[completed].add(e)
	if 1 != q.queues[completed].Len() {
		t.Error("Completed queue length should be one.")
	}

	q.Prune(time.Nanosecond)
	time.Sleep(time.Microsecond)
	if 0 != q.queues[completed].Len() {
		t.Error("Completed queue length should be zero after prune.")
	}
}

func TestQ_Queue(t *testing.T) {
	q := NewQueue()
	q.pipeline.stopped = true

	id := "foo"
	e := NewEntry(id)

	err := q.Queue(e)
	if err != ErrQupidProcessingStopped {
		t.Error("Should have ", ErrQupidProcessingStopped)
	}

	q.pipeline.stopped = false
	err = q.Queue(e)
	if err != nil {
		t.Error("Should no have an error but have ", err)
	}

	if 1 != q.queues[queued].Len() {
		t.Error("1 entry should be queued.")
	}

	err = q.Queue(e)
	if err != nil {
		t.Error("Should no have an error but have ", err)
	}

	if 1 != q.queues[queued].Len() {
		t.Error("1 entry should be queued on duplicate addition.")
	}

	q.Complete(NewResult(e))
	if 0 != q.queues[queued].Len() {
		t.Error("0 queued entries should exist after complete.")
	}

	if 1 != q.queues[completed].Len() {
		t.Error("1 completed entry should exist after complete.")
	}

	err = q.Queue(e)
	if err != nil {
		t.Error("Should no have an error but have ", err)
	}

	if 0 != q.queues[queued].Len() {
		t.Error("0 entries should be queued as duplicate entry id is completed already.")
	}
}

func TestQ_Results(t *testing.T) {
	q := NewQueue()
	r := q.Results()
	if "qupid.result" != reflect.TypeOf(r).Elem().String() {
		t.Error("Unexpected chan returned.")
	}

	if "chan" != reflect.TypeOf(r).Kind().String() {
		t.Error("Unexpected type returned.")
	}
}

func TestQ_Remove(t *testing.T) {
	q := NewQueue()
	e := NewEntry("foo")
	q.Queue(e)
	if 1 != q.queues[queued].Len() {
		t.Error("Queued length should be 1.")
	}

	q.Remove(e)
	if 0 != q.queues[queued].Len() {
		t.Error("Queued length should be 0 after remove.")
	}
}

func TestQ_RemoveByID(t *testing.T) {
	q := NewQueue()
	id := "baz"
	e := NewEntry(id)
	q.Queue(e)
	if 1 != q.queues[queued].Len() {
		t.Error("Queued length should be 1.")
	}

	q.RemoveByID(id)
	if 0 != q.queues[queued].Len() {
		t.Error("Queued length should be 0 after remove by id.")
	}
}

func TestQ_Stop(t *testing.T) {
	q := NewQueue()
	if q.pipeline.stopped {
		t.Error("Queue pipeline should not be stopped on new.")
	}

	q.Stop()
	if !q.pipeline.stopped {
		t.Error("Queue pipeline should be stopped on stop().")
	}
}

func TestQ_Working(t *testing.T) {
	q := NewQueue()
	id := "foo"
	e := NewEntry(id)
	q.Queue(e)
	tk := time.NewTicker(time.Second)
	var ee *Entry
	for {
		select {
		case ee = <-q.Consume():
			tk.Stop()
			goto confirm

		case <-tk.C:
			t.Error("Consume channel not processing.")
			return
		}
	}

confirm:
	if !reflect.DeepEqual(e, ee) {
		t.Error("Consumed Entry not the same.")
	}
}

type entity bool

func TestWithEntity(t *testing.T) {
	e := NewEntry("with")
	ent := entity(true)
	WithEntity(ent)(e)
	if !reflect.DeepEqual(ent, e.Entity) {
		t.Error("Entry entity not the same.")
	}
}

func TestNewEntry(t *testing.T) {
	id := "yuss"
	ent := entity(false)
	e := NewEntry(id, WithEntity(ent))
	if "qupid.Entry" != reflect.TypeOf(e).Elem().String() {
		t.Error("Unexpected typeof returned.")
	}

	if !reflect.DeepEqual(id, e.ID) {
		t.Error("Entry id not the same.")
	}
	if !reflect.DeepEqual(ent, e.Entity) {
		t.Error("Entry entity not the same.")
	}
}

func TestEntry_IsCompleted(t *testing.T) {
	e := NewEntry("foo")
	if e.IsCompleted() {
		t.Error("An entry should not be completed by default")
	}

	e.completed = time.Now()
	if !e.IsCompleted() {
		t.Error("This entry should be completed.")
	}
}

func TestEntry_IsProcessing(t *testing.T) {
	e := NewEntry("foo")
	if e.IsProcessing() {
		t.Error("An entry should not be processing by default")
	}

	e.processing = time.Now()
	if !e.IsProcessing() {
		t.Error("This entry should be processing.")
	}
}

func TestResult(t *testing.T) {
	id := "yuss"
	ent := entity(false)
	e := NewEntry(id, WithEntity(ent))
	r := NewResult(e)

	if "qupid.result" != reflect.TypeOf(r).String() {
		t.Error("Unexpected result typeof returned.")
	}

	if !reflect.DeepEqual(r.Entry, e) {
		t.Error("Result entry is not the same.")
	}

	if r.Err != nil {
		t.Error("Result should not have an error by default.")
	}
}

func TestIsQupidError(t *testing.T) {
	if IsQupidError(errors.New("foobar")) {
		t.Error("Regular error is not a cupid error!")
	}

	if !IsQupidError(ErrQupidPipelineBufferFull) {
		t.Error("ErrQupidPipelineBufferFull is a cupid error.")
	}

	if !IsQupidError(ErrQupidProcessingStopped) {
		t.Error("ErrQupidProcessingStopped is a cupid error.")
	}
}
