package qupid

type (
	// result is a struct that can optionally be passed to the Q.Complete() call. This allows
	// for listeners to act on potential errors encountered on processing.
	result struct {
		Entry *Entry
		Err   error
	}
)

// NewResult will return a result instance. A valid entry instance is required to create a result.
func NewResult(e *Entry) result {
	return result{
		Entry: e,
	}
}
